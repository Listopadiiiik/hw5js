// Приклад картки не завантажується, сподіваюсь, що виглядає нормально.

let users = 'https://ajax.test-danit.com/api/json/users'
let posts = 'https://ajax.test-danit.com/api/json/posts'

class Card {
    constructor(user, post){
        this.user = user
        this.post = post
    }
    render(){

        let trigger = false;

        fetch(this.post).then((response) => response.json()).then((data) => {
            // console.log(data)
            data.forEach((item) => {
                document.querySelector('.root').insertAdjacentHTML('afterbegin', `<div class="container"></div>`)
                document.querySelector('.container').insertAdjacentHTML('afterbegin', `<p class="user-${item.userId}"></p><p class="email-${item.userId}"></p>`)
                document.querySelector('.container').insertAdjacentHTML('beforeend', `<p class="title">${item.title}</p>`)
                document.querySelector('.title').insertAdjacentHTML('beforeend', `<p>${item.body}</p>`)
                document.querySelector('.container').insertAdjacentHTML('beforeend', `<button class="delete-btn" type="submit">Remove post</button>`)

                fetch(this.user).then((response) => response.json()).then((data) => {
                    // console.log(data)
                    data.forEach((item2) => {
                        if(item2.id == item.userId){
                            document.querySelectorAll(`.user-${item.userId}`).forEach((i) => i.innerHTML = item2.name)
                            document.querySelectorAll(`.email-${item.userId}`).forEach((i) => i.innerHTML = item2.email)
                        }
                    })
                })

                fetch(`${this.post}/${item.id}`, {
                    method: 'DELETE'
                })
                .then((response) => response.ok ? trigger = true : false)

                document.querySelectorAll('.delete-btn').forEach((button) => button.addEventListener('click', function(e){
                    
                    trigger == true ? e.target.parentElement.remove() : false;
                }))

            })
        })

        
    }

}
new Card(users, posts).render()

document.querySelectorAll('.delete-btn').forEach(function(el){
    el.addEventListener('click', function(e){
        e.target.parentElement.remove()
    })
})